'use strict';

var gulp = require('gulp');
var gutil = require('gulp-util');
var del = require('del');

// Load plugins
var $ = require('gulp-load-plugins')();
var sass = require('gulp-sass');
var cssGlobbing = require('gulp-css-globbing');
var babel = require('gulp-babel');
var babelify = require('babelify');
var browserify = require('browserify');
var watchify = require('watchify');
var sourcemaps = require('gulp-sourcemaps');
var buffer = require('vinyl-buffer');
var source = require('vinyl-source-stream'),
		sourceFile = './app/scripts/app.js',
		destFolder = './dist/scripts',
		destFileName = 'app.js';

var uglify = require('gulp-uglify');

var browserSync = require('browser-sync');
var reload = browserSync.reload;

// Styles
gulp.task('styles', ['sass']);
gulp.task('sass', function () {
	return gulp.src([
			'app/styles/**/*.scss',
			'app/styles/**/*.css'
		])
		.pipe(cssGlobbing({ extensions: ['.css', '.scss']}))
		.pipe(sass().on('error', sass.logError))
		.pipe($.autoprefixer('last 1 version'))
		.pipe(gulp.dest('dist/styles'))
		.pipe($.size());
});

var b = browserify({
		entries: [sourceFile],
		debug: true,
		insertGlobals: true,
		cache: {},
		packageCache: {},
		fullPaths: true
}).transform(babelify.configure({
	ignore: /node_modules/
}));

var bundler = watchify(b);

bundler.on('update', rebundle);
bundler.on('log', $.util.log);

function rebundle() {
	return bundler
		.bundle()
		// log errors if they happen
		.on('error', $.util.log.bind($.util, 'Browserify Error'))
		.pipe(source(destFileName))
		.pipe(buffer())
		.pipe(sourcemaps.init({	loadMaps: true })) // loads map from browserify file
			.pipe(uglify())
			.on('error', gutil.log)
		.pipe(sourcemaps.write('./')) // writes .map file
		.pipe(gulp.dest(destFolder))
		.on('end', function() {
				reload();
		});
}

// Scripts
gulp.task('scripts', rebundle);

gulp.task('buildScripts', function () {
	return browserify(sourceFile)
		.transform(babelify)
		.bundle()
		.pipe(gulp.dest('dist/scripts'));
});

// HTML
gulp.task('html', function () {
	return gulp.src('app/*.html')
		.pipe($.useref())
		.pipe(gulp.dest('dist'))
		.pipe($.size());
});

// Images
gulp.task('images', function () {
	return gulp.src('app/images/**/*')
		.pipe($.cache($.imagemin({
			optimizationLevel: 3,
			progressive: true,
			interlaced: true
		})))
		.pipe(gulp.dest('dist/images'))
		.pipe($.size());
});

// Clean
gulp.task('clean', function (cb) {
	$.cache.clearAll();
	cb(del.sync(['dist/styles', 'dist/scripts', 'dist/images']));
});

// Bundle
gulp.task('bundle', ['styles', 'scripts'], function () {
	return gulp.src('./app/*.html')
		.pipe($.useref.assets())
		.pipe($.useref.restore())
		.pipe($.useref())
		.pipe(gulp.dest('dist'));
});

gulp.task('buildBundle', ['styles', 'buildScripts'], function () {
	return gulp.src('./app/*.html')
		.pipe($.useref.assets())
		.pipe($.useref.restore())
		.pipe($.useref())
		.pipe(gulp.dest('dist'));
});


gulp.task('json', function () {
	gulp.src('app/scripts/json/**/*.json', {
			base: 'app/scripts'
		})
		.pipe(gulp.dest('dist/scripts/'));
});

// Robots.txt and favicon.ico
gulp.task('extras', function () {
	return gulp.src(['app/*.txt', 'app/*.ico'])
		.pipe(gulp.dest('dist/'))
		.pipe($.size());
});

// Watch
gulp.task('watch', ['html', 'bundle'], function () {
	browserSync({
		notify: false,
		logPrefix: 'BS',
		https: false,
		server: ['dist', 'app']
	});
	// Watch .json files
	gulp.watch('app/scripts/**/*.json', ['json']);
	// Watch .html files
	gulp.watch('app/*.html', ['html']);
	gulp.watch(['app/styles/**/*.scss', 'app/styles/**/*.css'], ['styles', reload]);
	// Watch image files
	gulp.watch('app/images/**/*', reload);
});

// Build
gulp.task('build', ['html', 'buildBundle', 'images','extras'], function () {
	gulp.src('dist/scripts/app.js')
		.pipe($.uglify())
		.pipe($.stripDebug())
		.pipe(gulp.dest('dist/scripts'));
});

// Default task
gulp.task('default', ['clean', 'build']);
