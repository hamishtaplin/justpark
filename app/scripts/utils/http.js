'use strict';

module.exports = class Http {

	static get(url) {
		return new Promise(function(resolve, reject) {

			const req = new XMLHttpRequest();

			req.open('GET','http://justpark.hamishtaplin.com/proxy.php?url=https://www.justpark.com'+url);

			req.onload = function() {
				if (req.status == 200) {
					resolve(req.response);
				}
				else {
					reject(Error(req.statusText));
				}
			};

			// Handle network errors
			req.onerror = function() {
				reject(Error("Network Error"));
			};

			// Make the request
			req.send();
		});
	}
}
