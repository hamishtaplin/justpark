"use strict";

import React from 'react/addons';

const ReactCSSTransitionGroup = React.addons.CSSTransitionGroup;

module.exports = React.createClass({

	render: function() {
		return (
			<div className="loader">
				<img src="/images/loader.png" alt="Loading" className="spin"/>
			</div>
		);
	}

});
