'use strict';

import React from 'react/addons';
import ReactGoogleMaps from 'react-googlemaps';
import Actions from './../actions';
import Loader from './Loader';

const GoogleMapsAPI = window.google.maps;
const GoogleMap = ReactGoogleMaps.Map;
const Marker = ReactGoogleMaps.Marker;
const OverlayView = ReactGoogleMaps.OverlayView;
const ReactCSSTransitionGroup = React.addons.CSSTransitionGroup;

export default Map = React.createClass({
	componentDidMount: function() {
		// scope this here so we can destroy/rebuild it later
		this.directionsDisplay = undefined;
		// listen for changes to sidebar
		Actions.sidebar.listen( ( state ) => {
			if (state === "closed") {
				this.setMapFocus("focused");
			}
		});

		Actions.locationSelected.listen( (markerId) => {
			if (markerId !== this.state.currentMarkerId) {
				this.focusOnMarker(this.state.markers[markerId]);
			}
		});

	},
	getInitialState: function() {
		return {
			coords: {},
			data: [],
			markers: [],
			loaded: false,
			focus: "focused",
			currentMarkerId: 0
		};
	},
	componentWillReceiveProps: function(nextProps) {
		this.setState({
			coords: nextProps.coords,
			data: nextProps.data,
			centered: false
		});
	},

	setMapFocus: function(focus) {
		// there's probably a better way of getting this node
		const mapNode = document.getElementById("map");

		if (mapNode !== null) {
			mapNode.addEventListener("transitionend", this.handleMapTransitionEnd);
		}

		// toggle this state to resize map
		this.setState({ focus: focus });

	},

	handleMapTransitionEnd: function(e) {
		// let the map know it's been resized
		GoogleMapsAPI.event.trigger(this.state.googleMap, 'resize');
		// re-center

		this.state.googleMap.fitBounds(this.latlngbounds);

		e.target.removeEventListener("transitionend", this.handleMapTransitionEnd);
	},

	focusOnMarker: function(marker) {

		// hack to get around the fact the ReactGoogleMaps Marker doesn't expose the original marker
		let markerPosition = (typeof marker.getPosition === 'function')
			? marker.getPosition()
			: marker;

		const directionsService = new GoogleMapsAPI.DirectionsService();

		const request = {
			origin : markerPosition,
			destination : new GoogleMapsAPI.LatLng(this.props.coords.lat, this.props.coords.lng),
			travelMode : GoogleMapsAPI.TravelMode.WALKING
		};

		this.setState({currentMarkerId: marker.zIndex})

		if (marker.zIndex) {
			// dispatch the event
			Actions.locationSelected(marker.zIndex, true);
		}

		this.setMapFocus("unfocused");

		// clear existing routes by resetting the renderer
		if (this.directionsDisplay != null) {
			this.directionsDisplay.setMap(null);
			this.directionsDisplay = null;
		}

		this.directionsDisplay = new GoogleMapsAPI.DirectionsRenderer({
			suppressMarkers: true,
			hideRouteList: false,
			preserveViewport: true
		});

		this.directionsDisplay.setMap(this.state.googleMap);

		directionsService.route(request, (response, status) => {
			if (status == GoogleMapsAPI.DirectionsStatus.OK) {
				this.directionsDisplay.setDirections(response);
			}
		});

	},

	handleMarkerClick: function (a, marker) {
		this.focusOnMarker(marker);
	},

	fitBounds: function() {
		this.state.googleMap.fitBounds(this.latlngbounds);
	},

	handleTilesloaded: function(map) {
		this.state.googleMap = map;
		if (!this.state.loaded) {
			this.fitBounds.call(this);
			this.setState({loaded:true});
		}

	},

	render: function() {
		if (this.state.data.length === 0) {
			return (<Loader />);
		} else {
			this.latlngbounds = new GoogleMapsAPI.LatLngBounds();
			let zoom = (window.innerWidth < 700) ? 15 : 17;
			let destinationCoords = new GoogleMapsAPI.LatLng(this.state.coords.lat, this.state.coords.lng);
			let logoIcon = new GoogleMapsAPI.MarkerImage("/images/markers/marker-logo.png", null, null, null, new GoogleMapsAPI.Size(45,45));

			let classes = "map " + (this.state.loaded ? "is-loading" : "is-loaded");
					classes += " is-" + this.state.focus;

			this.latlngbounds.extend(destinationCoords);

			// create individual location markers
			const markers = this.props.data.map( (location, index) => {

				const letter = ("abcdefghijklmnopqrstuvwxyz").charAt(index);
				const icon = new GoogleMapsAPI.MarkerImage("/images/markers/marker-"+letter+".png", null, null, null, new GoogleMapsAPI.Size(45,45));
				const coords = new GoogleMapsAPI.LatLng(location.coords.lat, location.coords.lng);
				const marker = (
					<Marker
						position={coords}
						title={location.title}
						icon={icon}
						onClick={this.handleMarkerClick}
						key={location.href}
						optimized={false}
						zIndex={index}
					/>
				);

				this.latlngbounds.extend(coords);
				this.state.markers[index] = coords;
				return marker;
			});

			this.state.map = (
				<GoogleMap
					onTilesLoaded={this.handleTilesloaded}
					ref="myMap"
					className={"map-instance"}
					disableDefaultUI={true}
					initialZoom={zoom}
					initialCenter={destinationCoords}
					width={"100%"}
					height={"100%"}>

				<Marker
					ref="marker"
					position={destinationCoords}
					icon={logoIcon}
					optimized={false}
					width="45px"
					height="45px"
					flat={true}
				/>

				{markers}

			</GoogleMap>);

			return (
				<ReactCSSTransitionGroup transitionName="fadeIn" component="div" transitionAppear={true}>
					<div id="map" className={classes} key="map">
						{this.state.map}
					</div>
				</ReactCSSTransitionGroup>

			);
		}
	}
});
