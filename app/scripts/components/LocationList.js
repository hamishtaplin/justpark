'use strict';

import _ from 'underscore';
import React from 'react';
import LocationItem from './LocationItem';
import actions from './../actions';
import Scroll from 'animated-scrollto';

module.exports = React.createClass({

	componentDidMount: function() {
		actions.locationSelected.listen((markerId, transition) => {
			this.setState({
				selectedLocationId: markerId,
				transitionOffset: transition
			});
		});
	},

	getInitialState: function() {
		return {
			selectedLocationId: null,
			transitionOffset: false
		};
	},

	componentDidUpdate: function() {
		// definitely not the best way to do this but in the interest of getting it done quickly...
		const node = document.getElementById('location-list');
		const selectedNode = document.querySelector('.location.is-highlighted');
		if (selectedNode ) {
			if (this.state.transitionOffset) {
				window.setTimeout(Scroll(node, selectedNode.offsetTop, 500), 200);
			} else if (window.innerWidth < 700) {
				node.scrollTop = selectedNode.offsetTop;
			}

		}
	},

	handleSorting: function(e){
		// sort here one day...
	},

	render: function() {

		if (typeof this.props.data === 'undefined') {
			return null;
		}

		const locationsItems = this.props.data.map((location, index) => {

			let open = (index === this.state.selectedLocationId) ? true : false;

			return (
				<li className="item">
					<LocationItem key={location.href} data={location} id={index} open={open}/>
				</li>
			);
		});

		return (
			<div className="locations">

				<div className="locations__opts field">
					<label htmlFor="sorting" className="label">Sort by:</label>
					<select name="sorting" id="sorting" className="select" onChange={this.handleSorting}>
						<option value="distance">Distance</option>
						<option value="rating">Rating</option>
						<option value="reviews">Reviews</option>
						<option value="price">Price</option>
					</select>
				</div>

				<div className="locations__list" id="location-list">
					<ul className="bare-list">
						{locationsItems}
					</ul>
				</div>

			</div>
		);
	}

});
