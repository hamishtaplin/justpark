'use strict';

import React from 'react';
import ToggleButton from './ToggleButton';

const ReactCSSTransitionGroup = React.addons.CSSTransitionGroup;

module.exports = React.createClass({

	render: function() {
		return (
			<header className="header">
				<img src="/images/logo-light.svg" width="203" height="43" className="header__logo logo"/>

				<div className="header__btn">
					<ToggleButton />
				</div>
			</header>
		);
	}

});
