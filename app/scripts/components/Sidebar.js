'use strict';

import React from 'react';
import LocationList from './LocationList';
import Actions from './../actions';

module.exports = React.createClass({
	getInitialState: function() {
		return { status: "closed" };
	},
	componentDidMount: function() {
		Actions.locationSelected.listen( (msg) => {
			this.setState( { status:"opened" } )
		});
		Actions.sidebar.listen( ( state ) => {
			if (state != this.state.status) {
				this.setState( { status: state } );
			}
		});
	},
	render: function() {
		const classes = "sidebar is-" + this.state.status;

		Actions.sidebar(this.state.status);

		return (
			<div className={classes}>
				<LocationList data={this.props.data} />
			</div>
		);
	}
});
