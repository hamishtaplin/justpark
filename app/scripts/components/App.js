'use strict';

import React from 'react/addons';
import Loader from './Loader';
import Header from './Header';
import Sidebar from './Sidebar';
import Map from './Map';
import http from '../utils/http';

const ReactCSSTransitionGroup = React.addons.CSSTransitionGroup;

module.exports = React.createClass({

	getInitialState: function() {
		return { loaded: true };
	},

	componentDidMount: function() {
		http.get('/1.1/location?q=Wembley')

		.then(
			value => {
				var data = JSON.parse(value).contents;
				this.setState({
					coords: data.coords,
					data: data.data,
					loaded: true
				});
			},
			reason => {
				console.error(reason);
			});
	},

	render: function() {

		if (this.state.loaded) {
			return (
			<div className="wrapper">
				<Header />
				<Map data={this.state.data} coords={this.state.coords} />
				<Sidebar data={this.state.data} />
			</div>
		);
		} else {
			return (<Loader />);
		}

	}

});
