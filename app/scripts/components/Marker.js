"use strict";

import React from 'react';
import ReactGoogleMaps from 'react-googlemaps';
const GoogleMapMarker = ReactGoogleMaps.Marker;

module.exports = React.createClass({

	getInitialState: function() {
		return {
			blah:"bloop"
		};
	},

	componentDidMount: function() {
		this.mapMarker = undefined;
	},

	render: function() {
		let marker =
			<GoogleMapMarker
				position={this.props.position}
				icon={this.props.icon} />

		this.mapMarker = marker;
		return marker;
	}

});
