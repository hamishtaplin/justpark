'use strict';

import React from 'react';

module.exports = React.createClass({

	render: function() {

		let img = Math.round(this.props.data.rating);
		let label = this.props.data.count + " reviews";

		if (this.props.data.rating < 1) {
			img = "unrated";
			label = "no reviews yet";
		}

		return (
			<div className="rating">
				<img className="rating__stars" src={"/images/stars/stars-"+img+".svg"} width="69" height="13"/>
				<span className="rating__label">{label}</span>
			</div>
		);

	}

});
