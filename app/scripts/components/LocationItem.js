"use strict";

import slugify from 'slugify';
import React from 'react';
import Rating from './Rating';
import Loader from './Loader';
import http from '../utils/http';

import Actions from './../actions';

module.exports = React.createClass({

	getInitialState: function() {
		return {
			expanded: false,
			loaded:false,
			data: {}
		};
	},

	componentDidMount: function() {

	},

	handleClick: function(e) {
		// dispatch the event
		Actions.locationSelected(this.props.id, false);
	},

	render: function() {
		if (!this.state.loaded) {

			http.get(this.props.data.href)

			.then(
				value => {
					this.setState({
						loaded: true,
						data: JSON.parse(value).contents.data
					});
				},
				reason => {
					console.error(reason);
				});

			return (<Loader />);

		} else {

			const data = this.state.data;
			const openClass = this.props.open ? "is-open" : "is-closed";
			const letter = ("abcdefghijklmnopqrstuvwxyz").charAt(this.props.id);

			const features = data.facilities.map(function(feature, index){
				return (
					<li className="feature item" key={index}>
						<img className="feature__icon" src={"/images/icon-"+slugify(feature).toLowerCase()+".svg"} alt={feature} />
						<div className="feature__label">{feature}</div>
					</li>
					)
			});

			return (
				<article className={this.props.open ? "location is-highlighted" : "location"} onClick={this.handleClick}>

					<div className="location__overview">

						<div className="location__id">
							<img src={"/images/markers/marker-"+letter+".png"} alt={letter} className="location__marker"/>
						</div>

						<header className="location__header">
							<div className="location__title" >{data.title}</div>
							<div className="location__rating">
								<Rating data={data.feedback}/>
							</div>
							<div className="location__spaces"><span>{data.category} with {data.spaces_to_rent} spaces</span></div>
						</header>

						<div className="location__book">
							<div className="location__price">
								{data.display_price.formatted_price}
							</div>
							<a className="location__link" href="http://justpark.com" target="_blank">Book now</a>
						</div>

					</div>

					<div className={"location__detail " + openClass}>
						<div className="location__description">
							{data.description.replace(/[^\x00-\x80]/g, "")}
						</div>
						<div className="location__features">
							<div className="features__title">Features</div>
							<ul className="bare-list inline-list features__list">{features}</ul>
						</div>
					</div>

				</article>
			);

		}
	}

});
