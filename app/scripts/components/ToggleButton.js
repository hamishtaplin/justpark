"use strict";

import React from 'react';
import _ from "underscore";
import Actions from './../actions';

module.exports = React.createClass({
	getInitialState: function() {
		return { toggled:false };
	},

	componentDidMount: function() {
		Actions.sidebar.listen( ( state ) => {
			this.setState({ toggled: (state !== "closed") });
		});
	},

	handleClick: function(e) {
		this.setState({ toggled: !this.state.toggled });
		// this is tightly coupled but could be passed in as props to make this component more re-usable
		this.state.toggled ? Actions.sidebar("opened") : Actions.sidebar("closed");
	},

	render: function() {
		let classes = "btn btn--menu ";
				classes += this.state.toggled ? "is-toggled" : "";
		return (
			<button className={classes} onClick={_.debounce(this.handleClick, 200)}/>
		);
	}

});
