import React from 'react/addons';
import Stores from './stores';
import App from './components/App';

React.render(<App/>, document.getElementById('app'));
