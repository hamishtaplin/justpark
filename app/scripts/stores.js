'user strict';

import Reflux from 'reflux';
import actions from './actions';

module.exports = Reflux.createStore({

	init: function() {
		this.listenTo(actions.locationSelected, this.onLocationSelected);
	},

	onLocationSelected: function(marker) {
		// console.log("marker:");
		// console.log(marker);
	}

});
