'use strict';

import Reflux from 'Reflux';

module.exports = Reflux.createActions({
	locationSelected: {},
	sidebar: { children: [ "opened", "closed" ] }
});
