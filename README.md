# Just Park Test Application

Built with:

- ES6 (via Babel)
- ReactJS
- Browserify
- Gulp
- Scss

Just `npm install` and then `gulp watch` :)